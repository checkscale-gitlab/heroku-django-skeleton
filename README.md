# Django environment skelton for Heroku in docker

This is a framework for publishing a Django environment on heroku.

In the local environment, you can develop and check your work on the Docker environment.

## Demo
https://django-skeleton-manabe.herokuapp.com/

* This application is published by Heroku's free plan. As such, it will go into sleep mode if there is no access for 30 minutes. **You will need to wait for a certain amount of time** because it will need to be restarted when you access it after that.


## Author
### MANABE yusuke
* E-mail: manabe.engineer@gmail.com

## License

"Django environment skelton for Heroku in docker" is under [MIT license](https://en.wikipedia.org/wiki/MIT_License).

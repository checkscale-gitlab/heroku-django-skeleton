-include .env


## Initialize after cloning this repository -


.PHONY: init


init:
	cp -n .env.example .env


## Docker controll ---------------------


.PHONY: up build down stop destroy rebuild ps


up:
	docker-compose up -d
	@make ps

build:
	docker-compose build

down:
	docker-compose down

stop:
	docker-compose stop

destroy:
	docker-compose down --rmi all --volumes

rebuild:
	@make destroy
	@make build

ps:
	docker-compose ps


## Per-container operations ------------


.PHONY: logs-python bash-python restart-python printenv-python \
	logs-db bash-db restart-db printenv-db \


logs-python:
	docker-compose logs --tail="10" python

bash-python:
	docker-compose exec python bash

restart-python:
	docker-compose restart python

printenv-python:
	docker-compose exec python printenv


logs-db:
	docker-compose logs --tail="10" db

bash-db:
	docker-compose exec db bash

restart-db:
	docker-compose restart db

printenv-db:
	docker-compose exec db printenv


## Heroku operations -------------------

.PHONY: heroku-login heroku-apps \
	heroku-bash-stg heroku-restart-stg heroku-logs-stg \
	heroku-bash-prd heroku-restart-prd heroku-logs-prd \


heroku-login:
	docker-compose exec --user root python heroku login -i

heroku-apps:
	docker-compose exec --user root python heroku apps


heroku-bash-stg:
	docker-compose exec --user root python heroku run bash -a $(HEROKU_STG_APP_NAME)

heroku-restart-stg:
	docker-compose exec --user root python heroku restart -a $(HEROKU_STG_APP_NAME)

heroku-logs-stg:
	docker-compose exec --user root python heroku logs --tail -a $(HEROKU_STG_APP_NAME)


heroku-bash-prd:
	docker-compose exec --user root python heroku run bash -a $(HEROKU_PRD_APP_NAME)

heroku-restart-prd:
	docker-compose exec --user root python heroku restart -a $(HEROKU_PRD_APP_NAME)

heroku-logs-prd:
	docker-compose exec --user root python heroku logs --tail -a $(HEROKU_PRD_APP_NAME)
